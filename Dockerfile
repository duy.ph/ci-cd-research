FROM node:lts-alpine

RUN mkdir -p /app/node
WORKDIR /app/node

COPY package*.json ./
RUN npm install
COPY . . 

EXPOSE 8080

CMD ["node", "src/index.js"]